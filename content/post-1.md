+++
title = "Mini-Project: Static Website"
template = "page.html"
date = 2024-02-01T15:00:00Z
[taxonomies]
tags = ["IDS721", "zola", "static website"]
[extra]
summary = "Shortcuts of the website"
mathjax = "tex-mml"
+++

<!-- more -->

## Home Page (local set up)
![homepage](../website-home.png)

## Project Page Detail
![projectpage1](../website-project1.png)
![projectpage2](../website-project2.png)